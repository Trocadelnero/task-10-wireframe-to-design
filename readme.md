# Task 10: Wireframe to Design [ Group Task ]

## Instructions
Instructions
Use the layout and styling skills you’ve learnt to recreate the Wireframe provided.  
The design does not need to be 100% perfect but should show a good attempt to recreate as close as possible.  

## Description
I used a mix of flexbox and css-grid. Nav and site-header was provided as code-exapmle to use and was kept although I made some small edits.  
after site-header the page was put in a 4(fr)-column css-grid and divided in sections one-ten.  
root font-size is 1.5vw.  
vw used for most width-related.  
rem used for most height-related.  
I set the different grid-sections to display: content and * in sections to position: relative to manage positioning using top/left/right:  
```
/*---------------------------
  Layout - MAIN (Container) - Type: Grid
----------------------------*/
#wrapper {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
}
/*---------------------------
  Layout - Sections Type: Grid / Contents
----------------------------*/
section {
  display: contents;
}
section * {
  position: relative;
}
```  

No media queries used since lack of experience/knowledge/effort, and the page is fairly responsive as is.




